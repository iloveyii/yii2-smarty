<?php
namespace softhem\smarty;

use yii\web\AssetBundle;

/**
 * Softhem Smarty AssetBundle
 * @since 0.1
 */
class SmartyAsset extends AssetBundle
{
    public $sourcePath = '@vendor/softhem/yii2-smarty/html';
    public $css = [
        'https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700',
        'plugins/slider.layerslider/css/layerslider.css',
        'css/essentials.css',
        'css/layout.css',
        'css/header-1.css',
        'css/color_scheme/green.css'
    ];
    public $js = [
        'js/scripts.js',
        'plugins/slider.layerslider/js/layerslider_pack.js',
        'js/view/demo.layerslider_slider.js'
    ];
    public $depends = [
        'rmrevin\yii\fontawesome\AssetBundle',
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

}
