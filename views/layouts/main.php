<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

if (class_exists('frontend\assets\AppAsset')) {
    frontend\assets\AppAsset::register($this);
} else {
    app\assets\AppAsset::register($this);
}

softhem\smarty\SmartyAsset::register($this);
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/softhem/yii2-smarty/html');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel="icon" type="image/png" href="/images/favicon.png" />
        <meta name="description" content="My Task Manager is a task management online application that helps you organize your task fast and easily online from any device. My Task Manager is an online free web application." />
        <meta name="keywords" content="task management, task management web app, online task management tools, web based task management web app" />
        <?php $this->head() ?>
        <script type="text/javascript">var plugin_path = '<?=$directoryAsset?>/plugins/';</script>
    </head>
    <body>
        <?php $this->beginBody() ?>
            <div class="wrapper">
                <?= $this->render(
                    'header.php',
                    ['directoryAsset' => $directoryAsset]
                ) ?>

                <?= $this->render(
                    'carousel.php',
                    ['directoryAsset' => $directoryAsset]
                )
                ?>

                <?= $this->render(
                    'content.php',
                    ['content' => $content, 'directoryAsset' => $directoryAsset]
                ) ?>

                <?= $this->render(
                    'footer.php',
                    ['directoryAsset' => $directoryAsset]
                ) ?>
            </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
