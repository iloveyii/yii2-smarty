<?php

use yii\helpers\Url;
$menu = [
    'index'=>'',
    'services'=>'',
    'courses'=>'',
    'contact'=>'',
];
$id = Yii::$app->controller->action->id;
$menu[$id]='current';
?>


<div id="header" class="sticky shadow-after-3 clearfix">

    <!-- SEARCH HEADER -->
    <div class="search-box over-header">
        <a id="closeSearch" href="#" class="glyphicon glyphicon-remove"></a>

        <form action="page-search-result-1.html" method="get">
            <input type="text" class="form-control" placeholder="SEARCH" />
        </form>
    </div>
    <!-- /SEARCH HEADER -->

    <!-- TOP NAV -->
    <header id="topNav">
        <div class="container">

            <!-- Mobile Menu Button -->
            <button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
                <i class="fa fa-bars"></i>
            </button>

            <!-- BUTTONS -->
            <ul class="pull-right nav nav-pills nav-second-main">

                <!-- SEARCH -->
                <li class="search">
                    <a href="javascript:;">
                        <i class="fa fa-search"></i>
                    </a>
                </li>
                <!-- /SEARCH -->

                <!-- QUICK SHOP CART -->
                <li class="quick-cart">
                    <a href="#">
                        <span class="badge badge-aqua btn-xs badge-corner">2</span>
                        <i class="fa fa-shopping-cart"></i>
                    </a>
                    <div class="quick-cart-box">
                        <h4>Shop Cart</h4>

                        <div class="quick-cart-wrapper">

                            <a href="#"><!-- cart item -->
                                <img src="<?=$directoryAsset?>/images/demo/people/300x300/4-min.jpg" width="45" height="45" alt="" />
                                <h6><span>2x</span> RED BAG WITH HUGE POCKETS</h6>
                                <small>$37.21</small>
                            </a><!-- /cart item -->

                            <a href="#"><!-- cart item -->
                                <img src="<?=$directoryAsset?>/images/demo/people/300x300/5-min.jpg" width="45" height="45" alt="" />
                                <h6><span>2x</span> THIS IS A VERY LONG TEXT AND WILL BE TRUNCATED</h6>
                                <small>$17.18</small>
                            </a><!-- /cart item -->

                            <!-- cart no items example -->
                            <!--
                            <a class="text-center" href="#">
                                <h6>0 ITEMS ON YOUR CART</h6>
                            </a>
                            -->

                        </div>

                        <!-- quick cart footer -->
                        <div class="quick-cart-footer clearfix">
                            <a href="shop-cart.html" class="btn btn-primary btn-xs pull-right">VIEW CART</a>
                            <span class="pull-left"><strong>TOTAL:</strong> $54.39</span>
                        </div>
                        <!-- /quick cart footer -->

                    </div>
                </li>
                <!-- /QUICK SHOP CART -->

            </ul>
            <!-- /BUTTONS -->

            <!-- Logo -->
            <a class="logo pull-left" href="<?=Url::to(['site/index'])?>">
                <img src="/images/logo.png" alt="MTM Logo" />MTM
            </a>

            <div class="navbar-collapse pull-right nav-main-collapse collapse submenu-dark">
                <nav class="nav-main">

                    <ul id="topMain" class="nav nav-pills nav-main">
                        <li class="active"><!-- HOME -->
                            <a href="<?=Url::to(['site/index'])?>">
                                HOME
                            </a>
                        </li>
                        <li class=""><!-- PAGES -->
                            <?php if(Yii::$app->user->isGuest): ?>
                                <a class="" href="<?=Url::to(['site/login'])?>">
                                    SIGN IN
                                </a>
                            <?php else :?>
                                <a class="" href="<?=Url::to(['site/logout'])?>">
                                    SIGN OUT
                                </a>
                            <?php endif?>
                        </li>
                        <li class=""><!-- FEATURES -->
                            <?php if(Yii::$app->user->isGuest): ?>
                                <a class="" href="<?=Url::to(['site/signup'])?>">
                                    SIGN UP
                                </a>
                            <?php else :?>
                                <a class="" href="http://btask.softhem.se/">
                                    ADMIN
                                </a>
                            <?php endif?>
                        </li>
                    </ul>

                </nav>
            </div>


        </div>
    </header>
    <!-- /Top Nav -->

</div>


