<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

if (class_exists('frontend\assets\AppAsset')) {
    frontend\assets\AppAsset::register($this);
} else {
    app\assets\AppAsset::register($this);
}

softhem\assets\GreenishAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/softhem/yii2-greenish/web');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
            <div class="wrapper">

                <?= $this->render(
                    'header.php',
                    ['directoryAsset' => $directoryAsset]
                ) ?>

                <?= $this->render(
                    'content.php',
                    ['content' => $content, 'directoryAsset' => $directoryAsset]
                ) ?>

                <?= $this->render(
                    'footer.php',
                    ['directoryAsset' => $directoryAsset]
                ) ?>

            </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
