<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\authclient\widgets\AuthChoice;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
$clients= [
    'facebook' => 'info',
    'google'=> 'danger'
];
$str = '';
?>
<div class="col-md-6 col-md-offset-3" style="background-color: #FFF; border-radius: 3px;">
    <div class="site-login" style="padding: 35px; border: 3px solid #fafafa; margin:20px 0;">
        <h1><i class="fa fa-sign-in"></i> <?= Html::encode($this->title) ?></h1>
        <hr />
        <p>Please fill out the following fields to login:</p>

        <div class="row">
            <div class="col-lg-12">
                <?php $form = ActiveForm::begin(['id' => 'login-form', 'class' => 'form-horizontal']); ?>

                    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>

                    <?= $form->field($model, 'rememberMe')->checkbox() ?>

                    <div style="color:#999;margin:1em 0">
                        If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <hr /> <br />
                            <?= Html::submitButton('<i class="fa fa-sign-in"></i> Login', ['class' => 'btn btn-success btn-block', 'name' => 'login-button']) ?>
                        </div>
                    </div>

                    <div class="row">
                        <?php $authAuthChoice = AuthChoice::begin([
                            'baseAuthUrl' => ['site/auth']
                        ]); ?>

                        <?php foreach ($authAuthChoice->getClients() as $client): ?>
                            <div class="col-md-6" style="margin-bottom:14px;">
                                <?php $authAuthChoice->clientLink($client, '<i class="fa fa-'.$client->getName().'"></i> '. ucfirst($client->getName()), ['class'=>'btn btn-block btn-'.$clients[$client->getName()]]) ?>
                            </div>
                        <?php endforeach; ?>

                        <?php AuthChoice::end(); ?>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>