<div id="product-promotion" class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="section-title section-top">Courses</h1>
            </div> <!-- /.col-md-12 -->
        </div> <!-- /.row -->
        <div class="row">
            <div class="col-md-2 col-sm-3">
                <div class="hidden-xs">
                    <img src="<?= $directoryAsset ?>/images/courses/alquran2.jpg" alt="Nurani Qaeda" height="180">
                </div> <!-- /.item-small -->
            </div> <!-- /.col-md-2 -->
            <div class="col-md-8 col-sm-6">
                <div class="item-large">
                    <img height="400" src="<?= $directoryAsset ?>/images/courses/alquran1.jpg" alt="Al Quran">
                    <div class="item-large-content">
                        <div class="item-header">
                            <h2 class="pull-left">Learn Al Quran</h2>
                                <span class="pull-right">Rate: <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i></span>
                            <div class="clearfix"></div>
                        </div> <!-- /.item-header -->
                        <p>The Qur'an ("Qor-Ann") is a Message from Allah (swt) to humanity. It was transmitted to us in a chain starting from the Almighty Himself (swt) to the angel Gabriel to the Prophet Muhammad (saw). This message was given to the Prophet (saw) in pieces over a period spanning approximately 23 years (610 CE to 632 CE). The Prophet (saw) was 40 years old when the Qur'an began to be revealed to him, and he was 63 when the revelation was completed. The language of the original message was Arabic, but it has been translated into many other languages. </p>
                        <p>The Qur'an is one of the two sources which form the basis of Islam. The second source is the Sunnah of the Prophet (saw). What makes the Qur'an different from the Sunnah is primarily its form. Unlike the Sunnah, the Qur'an is literally the Word of Allah (swt), whereas the Sunnah was inspired by Allah but the wording and actions are the Prophet's (saw). The Qur'an has not been expressed using any human's words. Its wording is letter for letter fixed by no one but Allah. </p>
                        <p>Prophet Muhammad (saw) was the final Messenger of Allah to humanity, and therefore the Qur'an is the last Message which Allah (swt) has sent to us. Its predecessors such as the Torah, Psalms, and Gospels have all been superseded. It is an obligation - and blessing - for all who hear of the Qur'an and Islam to investigate it and evaluate it for themselves. Allah (swt) has guaranteed that He will protect the Qur'an from human tampering, and today's readers can find exact copies of it all over the world. The Qur'an of today is the same as the Qur'an revealed to Muhammad (saw) 1400 years ago. </p>
                    </div> <!-- /.item-large-content -->
                </div> <!-- /.item-large -->
            </div> <!-- /.col-md-8 -->
            <div class="col-md-2 col-sm-3">
                <div class="hidden-xs">
                    <img src="<?= $directoryAsset ?>/images/courses/alquran2.jpg" alt="Al Quran" height="180">
                </div> <!-- /.item-small -->
            </div> <!-- /.col-md-2 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div> <!-- /#product-promotion -->

<div id="courses" class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <br />
                <hr />
                <br /><br />
            </div> <!-- /.col-md-12 -->
        </div> <!-- /.row -->
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="product-item">
                    <div class="item-thumb">
                        <span class="note"><img src="<?= $directoryAsset ?>/images/small_logo_1.png" alt=""></span>
                        <div class="overlay">
                            <div class="overlay-inner">
                                <a href="#nogo" class="view-detail">Learn More</a>
                            </div>
                        </div> <!-- /.overlay -->
                        <img src="<?= $directoryAsset ?>/images/products/product1.jpg" alt="">
                    </div> <!-- /.item-thumb -->
                    <h3>Qur’an Recitation</h3>
                        <span>Correcting Pronunciation (Makharij) of Arabic alphabets, learning Tajweed rules &
                            practicing recitation extensively. For Beginner, Intermediate & Advance students. </span>
                </div> <!-- /.product-item -->
            </div> <!-- /.col-md-3 -->
            <div class="col-md-3 col-sm-6">
                <div class="product-item">
                    <div class="item-thumb">
                        <span class="note"><img src="<?= $directoryAsset ?>/images/small_logo_3.png" alt=""></span>
                        <div class="overlay">
                            <div class="overlay-inner">
                                <a href="#nogo" class="view-detail">Learn More</a>
                            </div>
                        </div> <!-- /.overlay -->
                        <img src="<?= $directoryAsset ?>/images/products/product2.jpg" alt="">
                    </div> <!-- /.item-thumb -->
                    <h3>Qur’an Memorization</h3>
                        <span>Help in memorizing individual Surahs or Qur’an as a whole in the right way via
                            the most effective classical methods taught by our Tutors.</span>
                </div> <!-- /.product-item -->
            </div> <!-- /.col-md-3 -->
            <div class="col-md-3 col-sm-6">
                <div class="product-item">
                    <div class="item-thumb">
                        <span class="note"><img src="<?= $directoryAsset ?>/images/small_logo_2.png" alt=""></span>
                        <div class="overlay">
                            <div class="overlay-inner">
                                <a href="#nogo" class="view-detail">Learn More</a>
                            </div>
                        </div> <!-- /.overlay -->
                        <img src="<?= $directoryAsset ?>/images/products/product3.jpg" alt="">
                    </div> <!-- /.item-thumb -->
                    <h3>Qur’anic Arabic</h3>
                        <span>Learn the language of the glorious Qur’an so that you can understand its meaning
                            directly from the Arabic text, from our expert Tutors.</span>
                </div> <!-- /.product-item -->
            </div> <!-- /.col-md-3 -->
            <div class="col-md-3 col-sm-6">
                <div class="product-item">
                    <div class="item-thumb">
                        <span class="note"><img src="<?= $directoryAsset ?>/images/small_logo_1.png" alt=""></span>
                        <div class="overlay">
                            <div class="overlay-inner">
                                <a href="#nogo" class="view-detail">Learn More</a>
                            </div>
                        </div> <!-- /.overlay -->
                        <img src="<?= $directoryAsset ?>/images/products/product4.jpg" alt="">
                    </div> <!-- /.item-thumb -->
                    <h3>Qur’an Hifz</h3>
                        <span>Special Word By Word Quran Recitation, great for learning tajweed & reading . The program aims
                            to simulate a live teacher as closely as possible.</span>
                </div> <!-- /.product-item -->
            </div> <!-- /.col-md-3 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div> <!-- /#products -->