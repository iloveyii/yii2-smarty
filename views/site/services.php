<div id="services" class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="section-title section-top">SERVICES</h1>
            </div> <!-- /.col-md-12 -->
        </div> <!-- /.row -->
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="service-item">
                    <span class="service-icon first"></span>
                    <h3>One to one live classes</h3>
                    <p>All of our courses are conducted online with one on one Real time tutoring.</p>
                </div> <!-- /.service-item -->
            </div> <!-- /.col-md-4 -->
            <div class="col-md-4 col-sm-6">
                <div class="service-item">
                    <span class="service-icon second"></span>
                    <h3>Female Tutors</h3>
                    <p>Classes are also offered by female teachers, who are required to underago a very intensive training by our recruiting panel.</p>
                </div> <!-- /.service-item -->
            </div> <!-- /.col-md-4 -->
            <div class="col-md-4 col-sm-6">
                <div class="service-item">
                    <span class="service-icon third"></span>
                    <h3>Anytime Any Device</h3>
                    <p>Connecting to students anywhere in the world. Our classes can be taken anytime and on any device including Laptop, Tablet and android Phones.</p>
                </div> <!-- /.service-item -->
            </div> <!-- /.col-md-4 -->

            <div class="col-md-4 col-sm-6">
                <div class="service-item">
                    <span class="service-icon fourth"></span>
                    <h3>Revision Made Easy</h3>
                    <p>Live recordings of lessons as it happnes, inculding audio & video inputs, for revision purpose.</p>
                </div> <!-- /.service-item -->
            </div> <!-- /.col-md-4 -->
            <div class="col-md-4 col-sm-6">
                <div class="service-item">
                    <span class="service-icon fourth"></span>
                    <h3>Virtual Classrooms</h3>
                    <p>A true learning experience with tools like multi-way audio, video streams, integrated chat, online whiteboard, application sharing, file transfer, and more</p>
                </div> <!-- /.service-item -->
            </div> <!-- /.col-md-4 -->
            <div class="col-md-4 col-sm-6">
                <div class="service-item">
                    <span class="service-icon fourth"></span>
                    <h3>Flexible Scheduling</h3>
                    <p>Classes are offered 24 hrs a day and 7 days a week, with a flexibility to choose timings that is convenient to Students.</p>
                </div> <!-- /.service-item -->
            </div> <!-- /.col-md-4 -->
        </div> <!-- /.row -->

    </div> <!-- /.container -->
</div> <!-- /#services -->