<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\web\View;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;

?>
<div id="contact" class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="section-title section-top">Contact</h1>
            </div> <!-- /.col-md-12 -->
        </div> <!-- /.row -->
        <div class="row">
            <div class="col-md-offset-2 col-md-8 text-center bigger-text">
                <p>Send your question by filling the following form.</p>
            </div>
            <div class="col-md-6 col-sm-6">
                <div id="map" class="light-green-border">
                </div>
            </div> <!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6">

                <div class="row contact-form">

                    <fieldset class="col-md-6 col-sm-6">
                        <input id="name" type="text" name="name" placeholder="Name">
                    </fieldset>
                    <fieldset class="col-md-6 col-sm-6">
                        <input type="email" name="email" id="email" placeholder="Email">
                    </fieldset>
                    <fieldset class="col-md-12">
                        <input type="text" name="subject" id="subject" placeholder="Subject">
                    </fieldset>
                    <fieldset class="col-md-12">
                        <textarea name="comments" id="comments" placeholder="Message"></textarea>
                    </fieldset>
                    <fieldset class="col-md-12">
                        <input type="submit" name="send" value="Send Message" id="submit" class="button">
                    </fieldset>

                </div> <!-- /.contact-form -->

            </div> <!-- /.col-md-6 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div>

<?php
$this->registerJs("
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' +
        'callback=initialize';
    document.body.appendChild(script);
", View::POS_END, 'google-map');
?>
