<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-6 col-md-offset-3" style="background-color: #FFF; border-radius: 3px;">
    <div class="site-signup" style="padding:30px; border: 3px solid #fafafa; margin:20px 0;">
        <h1><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h1>
        <hr />
        <p>Please fill out the following fields to signup:</p>

        <div class="row">
            <div class="col-lg-12">
                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'email') ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>

                    <br /><hr />
                    <div class="form-group">
                        <?= Html::submitButton('<i class="fa fa-pencil"></i> Signup ', ['class' => 'btn btn-success', 'name' => 'signup-button']) ?>
                        <?= Html::submitButton('<i class="fa fa-facebook"></i> Facebook', ['class' => 'btn btn-info', 'name' => 'signup-button']) ?>
                        <?= Html::submitButton('<i class="fa fa-google"></i> Google  ', ['class' => 'btn btn-danger', 'name' => 'signup-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
